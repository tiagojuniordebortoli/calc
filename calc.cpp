#include <iostream>

namespace math {
	double square(double n) 
	{
		return n*n;
	}

	double sqrt(double n) 
	{
		auto old = 0.0;
		auto guess = 1.0;

		while (old != guess) {
			old = guess;
			guess = (guess + n / guess) / 2;
		}
		return guess;
	}

	double power(double b, int exp) 
	{
		auto answer = 1.0;
		for (auto i = 0; i < exp; i++) {
			answer *= b;
		}

		return answer;
	}

	namespace recursive {
		double power(double b, int exp) 
		{
			if (exp == 0) {
				return 1;
			}
			auto half = exp / 2;
			auto half_answer = power(b, half);
			auto answer = half_answer * half_answer;

			if (exp % 2 == 1) {
				answer *= b;

			}
			return answer;
		}
	}

}

int main()
{
	using namespace math;

	std::cout << math::sqrt(square(42)) << '\n'; 
	std::cout << "1 to de power 1000000000:" << power(1, 1000000000) << '\n';
	std::cout << "1 to de power 1000000000:" << recursive::power(1, 1000000000) << '\n';

}